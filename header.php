<?php
/**
 * Pagina do Topo
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php
	/* Print the <title> tag based on what is being viewed.	 */	
	global $page, $paged;

	wp_title( ' :: ', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ){
		echo " :: $site_description";
	}

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 ){
		echo ' :: ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
		}
	?>
</title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/util.js"></script>

<?php 
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>

</head>

<body>
<div id="bgGeral">
<div id="bgTopo">
  <div id="topo">
    <div id="logo"> <a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/logoPT.png" /></a> </div>
    <!-- logo -->
    <div id="imgsTopo"></div>
  </div>
  <!-- topo --> 
</div>
<!-- bgTopo -->

<div id="bgMenu">
  <?php wp_nav_menu( array( 'container_class' => 'menu-central', 'theme_location' => 'primary' ) ); ?>
</div>
<!-- bgMenu -->
<div id="linha"></div>

 <div id="bgCorpo">
            <div id="corpo">
