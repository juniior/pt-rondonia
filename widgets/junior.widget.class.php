<?php

/**
 * Widget de para Ativar a Enquete
 * 
 * @author Junior Santos <juniior.pvh@gmail.com>
 */
class Enquete extends WP_Widget {
	
	/**
	 * Construtor
	 */
	public function Enquete() {
		//parent::WP_Widget(false, $name = '(PT) Enquete');

		$widget_ops = array( 'description' => __('Use este widget para adicinar a area de Enquete na lateral do site!') );
		parent::__construct( false, __('(PT) Area da Enquete'), $widget_ops );
	}
	
	/**
	 * Exibição final do Widget (já no sidebar)
	 *
	 * @param array $argumentos Argumentos passados para o widget
	 * @param array $instancia Instância do widget
	 */
	public function widget($argumentos, $instancia) {
		?>
		<div class="titulo">Enquete</div>
        <div id="enquete">
        	<?php if (function_exists('vote_poll') && !in_pollarchive()): get_poll(); endif; ?>
        </div><!-- enquete -->
	<?php
	}
	
	/**
	 * Salva os dados do widget no banco de dados
	 *
	 * @param array $nova_instancia Os novos dados do widget (a serem salvos)
	 * @param array $instancia_antiga Os dados antigos do widget
	 * 
	 * @return array $instancia Dados atualizados a serem salvos no banco de dados
	 */
	public function update($nova_instancia, $instancia_antiga) {
		
	}
	
	/**
	 * Formulário para os dados do widget (exibido no painel de controle)
	 *
	 * @param array $instancia Instância do widget
	 */
	public function form($instancia) {
		
	}
	
}

/**
 * Widget de para Ativar a Pesquisa
 * 
 * @author Junior Santos <juniior.pvh@gmail.com>
 */
class Pesquisa extends WP_Widget{
	/**
	 * Construtor
	 */
	public function Pesquisa() { parent::WP_Widget(false, $name = '(PT) Pesquisa Simples'); }

	/**
	 * Exibição final do Widget (já no sidebar)
	 *
	 * @param array $argumentos Argumentos passados para o widget
	 * @param array $instancia Instância do widget
	 */
	public function widget($argumentos, $instancia) {
 ?><div id="pesquisa">
            <h1>Pesquisa</h1> 
            <form role="search" id="searchform" method="get" action="<?php bloginfo('home'); ?>">
                <input type="text" name="s" id="pesquisa"  class="txt"/>
                <input type="submit" id="searchsubmit" value="" class="btn" />        
            </form>           
            <!-- <p><a href="">Pesquisa avançada</a></p> -->
        </div><!-- pesquisa --><?php
	}

	/**
	 * Salva os dados do widget no banco de dados
	 *
	 * @param array $nova_instancia Os novos dados do widget (a serem salvos)
	 * @param array $instancia_antiga Os dados antigos do widget
	 */
	public function update($nova_instancia, $instancia_antiga) {}

	/**
	 * Formulário para os dados do widget (exibido no painel de controle)
	 *
	 * @param array $instancia Instância do widget
	 */
	public function form($instancia) {	}
}


/**
 * Widget de para Ativar os Banners
 * 
 * @author Junior Santos <juniior.pvh@gmail.com>
 */
class Banners extends WP_Widget{
	/**
	 * Construtor
	 */
	public function Banners() { parent::WP_Widget(false, $name = '(PT) Banners'); }

	/**
	 * Exibição final do Widget (já no sidebar)
	 *
	 * @param array $argumentos Argumentos passados para o widget
	 * @param array $instancia Instância do widget
	 */
	public function widget($argumentos, $instancia) {
 ?>
  <div class="titulo">Divulgação</div>
        <div id="banners">
           <?php get_template_part( 'banners', 'banners' ); ?>
        </div><!-- banners -->
 	<?php
	}

	/**
	 * Salva os dados do widget no banco de dados
	 *
	 * @param array $nova_instancia Os novos dados do widget (a serem salvos)
	 * @param array $instancia_antiga Os dados antigos do widget
	 */
	public function update($nova_instancia, $instancia_antiga) {}

	/**
	 * Formulário para os dados do widget (exibido no painel de controle)
	 *
	 * @param array $instancia Instância do widget
	 */
	public function form($instancia) {		
  		get_template_part( 'banners', 'banners' ); 
	}
}

/**
 * Widget de para Ativar a AgendaEventos
 * 
 * @author Junior Santos <juniior.pvh@gmail.com>
 */
class AgendaEventos extends WP_Widget {
	
	/**
	 * Construtor
	 */
	public function AgendaEventos() {
		parent::WP_Widget(false, $name = '(PT) Agenda e Eventos');
	}
	
	/**
	 * Exibição final do Widget (já no sidebar)
	 *
	 * @param array $argumentos Argumentos passados para o widget
	 * @param array $instancia Instância do widget
	 */
	public function widget($argumentos, $instancia) {
		?>
		<div class="titulo">Agenda de Eventos</div>
        <div id="agenda">
            <?php query_posts("showposts=3&category_name=agenda"); ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="itemEvento">
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail( 'foto_noticia_destaque' ); ?>
                    <div class="data"><?php echo get_the_date('d/m/Y') ?></div>
                    <p><?php the_title();?></p>
                </a>
            </div>
            <?php endwhile; ?>
            
            <a href="?cat=<?php echo get_query_var('cat'); ?>" class="btnA" ></a>            
        </div>        <!-- agenda -->
	<?php
	}
	
	/**
	 * Salva os dados do widget no banco de dados
	 *
	 * @param array $nova_instancia Os novos dados do widget (a serem salvos)
	 * @param array $instancia_antiga Os dados antigos do widget
	 * 
	 * @return array $instancia Dados atualizados a serem salvos no banco de dados
	 */
	public function update($nova_instancia, $instancia_antiga) {
		
	}
	
	/**
	 * Formulário para os dados do widget (exibido no painel de controle)
	 *
	 * @param array $instancia Instância do widget
	 */
	public function form($instancia) {
		
	}
	
}
?>