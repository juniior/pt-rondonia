<?php
/**
 * The template for displaying Category Archive pages.
 * Pagina que chama a pagina de listagem
 * Aqui vo chamar a pagina de listagem para categorias que fica no arquivo lista.php 
 *	
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<?php get_template_part( 'lista', 'category' );	?>
  
<?php get_sidebar(); ?>
<?php get_footer(); ?>
