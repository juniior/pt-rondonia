 <?php
/**
 * Faz as paginação das paginas
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<div align="center" class="paginacao_lista paginacao"><?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?></div>