<?php
/**
 * Pagina Mostrar
 *
 * É o arquivo responsável por exibir a pagina do seu post, ex: http://site.com/nome-do-post/. 
 * Nele normalmente vem integrado com o arquivo comments.php que será abordado mais adiante.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
          
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div id="pgMostrar">
        <?php $categoria = get_the_category(); ?>
        <div class="titulo"><?php echo $categoria[0]->cat_name; ?></div>
        <div id="Mostrar">  
            <div id="Conteudo">
                    <?php the_post_thumbnail( 'album_foto' );?>
                    <div class="data"><?php echo $categoria[0]->cat_name; ?> > <?php echo $categoria[1]->cat_name; ?>: <?php the_modified_date('l, j \d\e F \d\e Y - G:i');//twentyten_posted_on(); ?></div>
                    <h2><?php the_title(); ?></h2>
                    <h3><?php the_content(); ?></h3>
                    <p>Fonte: <?php the_author_firstname()?>&nbsp;<?php the_author_lastname(); ?></p>
            </div><!-- Conteudo -->
        </div><!-- Mostrar -->
    </div><!-- pgMostrar -->
    <?php //comments_template( '', true ); ?>
    <?php endwhile; // end of the loop. ?>
               
<?php get_sidebar(); ?>            
<?php get_footer(); ?>
