<?php
/**
 * Faz as pesquisa e envia para pagida de listagem forma de listagem
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<?php if ( have_posts() ) : ?>
	
		<h2 class="titulo"><?php printf( __( 'Resultados encontrados para: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		<?php get_template_part( 'lista', 'category' ); ?>
	
	<?php else : ?>
				
	<div id="pgMostrar">
	    <div class="titulo"></div>
	    <div id="Mostrar">  
	        <div id="Conteudo">
	        	<h2><?php printf( __( 'Nada foi encontrado: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h2>								
				<p><?php _e( 'Desculpe, mas nada foi encontrado com os seus critérios de pesquisa. Por favor, tente novamente com algumas palavras-chave diferentes.', 'twentyten' ); ?></p>						
			</div><!-- Conteudo -->
	    </div><!-- Mostrar -->
	</div><!-- pgMostrar -->
					
	<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>