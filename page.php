<?php
/**
 * Pagina Mostrar
 *
 * Responsável pelo template das páginas. Isto é, por ele você define como as paginas do seu site serão exibidas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
          
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div id="pgMostrar">
        <div class="titulo"></div>
        <div id="Mostrar">  
            <div id="Conteudo">
                    <?php the_post_thumbnail( 'album_foto' );?>                    
                    <h2><?php the_title(); ?></h2><div class="data"><?php the_modified_date('l, j \d\e F \d\e Y - G:i');//twentyten_posted_on(); ?></div>
                    <h3><?php the_content(); ?></h3>
                    <p>Fonte: <?php the_author_firstname()?>&nbsp;<?php the_author_lastname(); ?></p>
            </div><!-- Conteudo -->
        </div><!-- Mostrar -->
    </div><!-- pgMostrar -->
    <?php //comments_template( '', true ); ?>
    <?php endwhile; // end of the loop. ?>
               
<?php get_sidebar(); ?>            
<?php get_footer(); ?>
