<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<div class="coluDir">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Sidebar Lateral Direita') ) : ?>
<?php endif; ?> 
</div>