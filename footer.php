<?php
/*
 *
 * Parte final de pagina
 *
*/
  wp_footer();
?>
      </div>            <!-- corpo -->
    </div>        <!-- bgCorpo -->
  <div id="bgRodape">
    <div id="rodape">
      <div class="endereco">
        <p class="orange">Partido dos Trabalhadores<br/>
          Diretório Estadual de Rondônia</p>
        <p class="white">Av. Calama, n°895 - Centro<br/>
          CEP 78903-000 - Porto Velho/RO</p>
      </div>
      <!-- endereco -->
      <div class="contato">
        <p class="orange">Contato:</p>
        <p class="white">Fone: +55 (69) 3224-5906 / 5926<br />
          E-mail:ptrondonia@ptrondonia.org.br<br />
        </p>
      </div>
      <!-- contato -->
      <input type="button" class="btnM"/>
      <div class="social">
        <ul>
          <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/BLOG.png" /></a></li>
          <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icoTw.png" /></a></li>
          <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icoFac.png" /></a></li>
          <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icoYou.png" /></a></li>
        </ul>
      </div>
      <!-- social --> 
    </div>
    <!-- rodape --> 
  </div><!-- bgRodape -->
</div><!-- bgGeral --> 

<script type="text/javascript">
    $(window).load(function () {           
            mostrar('1');           
        });
</script> 


</body></html>