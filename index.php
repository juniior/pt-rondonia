<?php
/**
 * The main template file.
 *
 * Pagina Inicial
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

    <div id="coluEsq">
        <div class="titulo">Destaques</div>
        <div id="destaque">
        <div id="slide_painel">
            <?php if (function_exists('nivoslider4wp_show')) { nivoslider4wp_show(); } ?>
        </div>
        </div><!-- destaque -->
 
        <div class="titulo">Avisos e Novidades</div>
        <div id="avisoNovidade">
            <div id="aDestaque">
                <?php query_posts("showposts=1&category_name=noticias"); $contador = 1; ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php $categoria = get_the_category(); ?>
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail( 'painel_destaque' );?>
                    <div class="data"><?php echo $categoria[0]->cat_name; ?> > <?php echo $categoria[1]->cat_name; ?>: <?php echo get_the_date('d/m/Y') ?></div>
                    <h2><?php the_title(); ?></h2>
                    <p><?php $txt =  substr(get_the_excerpt(), 0, 160); echo $txt . '...'; ?></p>
                </a>
                <?php endwhile; ?>
            </div>            <!-- aDestaque -->
            <div id="painelMenu">
                <ul>
                    <li><a href="javascript:;" onclick="mostrar('1');">Últimas</a></li>
                    <li><a href="javascript:;" onclick="mostrar('2');">Local</a></li>
                    <li><a href="javascript:;" onclick="mostrar('3');">Nacional</a></li>
                </ul>
            </div>
            <div id="listNovidades">
                <div id="ultimas">
                    <?php wp_reset_query(); $posts = query_posts("showposts=4&category_name=noticias"); $i = 0;                
                         while (have_posts()) : the_post();
                         if($i != 0){ //Condição para não mostrar a 1 noticia que está em destaque
                         $categoria = get_the_category(); 
                     ?>
                    <a href="<?php the_permalink(); ?>" class="itemListaNovidades">
                        <div class="data"><?php echo $categoria[0]->cat_name; ?> > <?php echo $categoria[1]->cat_name; ?>: <?php echo get_the_date('d/m/Y') ?></div>
                        <h2><?php the_title(); ?></h2>
                        <p><?php $txt = substr(get_the_excerpt(), 0, 160); echo $txt . '...'; ?></p>
                    </a>
                    <br />
                    <?php } $i++; endwhile; ?>                    
                </div><!-- ultimas -->

                <div id="locais">
                    <?php wp_reset_query(); $posts = query_posts("showposts=3&category_name=noticias-regional"); 
                            while (have_posts()) : the_post();
                            $categoria = get_the_category(); 
                        ?>
                    <a href="<?php the_permalink(); ?>" class="itemListaNovidades">
                        <div class="data"><?php echo $categoria[0]->cat_name; ?> > <?php echo $categoria[1]->cat_name; ?>: <?php echo get_the_date('d/m/Y') ?></div>
                        <h2><?php the_title(); ?></h2>
                        <p><?php $txt =  substr(get_the_excerpt(), 0, 160); echo $txt . '...'; ?></p>
                    </a>
                    <br />
                    <?php endwhile; ?>
                </div>                <!-- locais -->

                <div id="nacionais">                    
                     <?php wp_reset_query(); $posts = query_posts("showposts=3&category_name=noticias-nacional"); 
                            while (have_posts()) : the_post();
                            $categoria = get_the_category(); 
                        ?>
                    <a href="<?php the_permalink(); ?>" class="itemListaNovidades">
                        <div class="data"><?php echo $categoria[0]->cat_name; ?> > <?php echo $categoria[1]->cat_name; ?>: <?php echo get_the_date('d/m/Y') ?></div>
                        <h2><?php the_title(); ?></h2>
                        <p><?php $txt =  substr(get_the_excerpt(), 0, 160); echo $txt . '...'; ?></p>
                    </a>
                    <br />
                    <?php endwhile; ?>
                </div>                <!-- nacionais -->
            </div>            <!-- listNovidades -->
            <a href="?cat=<?php echo get_query_var('cat'); ?>" class="btnN" ></a>
        </div>        <!-- avisoNovidade -->

        <div class="titulo">Galeria Multimídia</div>
        <div id="galeriaMult">
            <h5>Últimas Atualizações</h5>
            <div class="fotos" id="itemMult">
                <h1 class="fotos">Fotos</h1>
                <?php wp_reset_query(); $posts = query_posts("showposts=1&category_name=fotos"); 
                    while (have_posts()) : the_post(); 
                    the_post_thumbnail( 'multimidia_fotos' );
                ?>
                <div class="data"><?php echo get_the_date('d/m/Y') ?></div>
                <p class="fotos"><?php the_title(); ?></p>                
                <a href="?cat=<?php echo get_query_var('cat'); ?>" class="btnF" ></a>
                <?php endwhile; ?>
            </div>

            <div class="videos" id="itemMult">
                <h1 class="videos">Videos</h1>
                <?php wp_reset_query(); $posts = query_posts("showposts=1&category_name=videos"); while (have_posts()) : the_post(); 
                    the_post_thumbnail( 'multimidia_fotos' );
                ?>
                <div class="data"><?php echo get_the_date('d/m/Y') ?></div>
                <p class="fotos"><?php the_title(); ?></p>
                <a href="?cat=<?php echo get_query_var('cat'); ?>" class="btnV" ></a>
                <?php endwhile; ?>
            </div>

            <div class="downloads" id="itemMult">
                <h1 class="downloads">Downloads</h1>
                <?php wp_reset_query(); $posts = query_posts("showposts=1&category_name=downloads"); while (have_posts()) : the_post(); 
                    the_post_thumbnail( 'multimidia_fotos' );
                ?>
                <div class="data"><?php echo get_the_date('d/m/Y') ?></div>
                <p class="fotos"><?php the_title(); ?></p>
                <a href="?cat=<?php echo get_query_var('cat'); ?>" class="btnD" ></a>
                <?php endwhile; ?>
            </div>
        </div><!-- galeriaMult --></div><!-- coluEsq -->
        
<?php get_sidebar(); ?>
<?php get_footer(); ?>