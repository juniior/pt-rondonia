<?php
/**
 * Mostra as categorias em forma de listagem
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

    <div id="pgListar">
        <div class="titulo"><?php single_cat_title(); ?></div>
        <div id="Listagem">
            <?php while ( have_posts() ) : the_post() ?>        
                <?php $categoria = get_the_category(); ?>         
                <div class="itemLista">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail( 'album_foto' );?><!--<img src="" />-->
                        <div class="data"><?php echo $categoria[0]->cat_name; ?> > <?php echo $categoria[1]->cat_name; ?>: <?php echo get_the_date('j \d\e F \d\e Y \a\s g:i a'); ?></div>
                        <h2><?php the_title(); ?> </h2>
                       <p><?php $txt =  substr(get_the_excerpt(), 0, 400); echo $txt . '...'; ?></p>                       
                    </a>
                </div> <!-- itemLista -->
            <?php endwhile; // End the loop. Whew. ?>
        </div> <!-- Listagem -->
        <?php get_template_part( 'paginacao', 'index' );  //get_paginacao(); ?>
    </div> <!-- pgListar -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
